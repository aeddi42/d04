/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:25:05 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:00:03 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.hpp"

				PlasmaRifle::PlasmaRifle(void) : AWeapon("Plasma Rifle", 5, 21) {}

				PlasmaRifle::PlasmaRifle(PlasmaRifle const & src) : AWeapon() {
	*this = src;
}

PlasmaRifle&	PlasmaRifle::operator=(PlasmaRifle const & rhs) {

	this->_name = rhs.getName();
	this->_apcost = rhs.getApcost();
	this->_damage = rhs.getDamage();

	return *this;
}

				PlasmaRifle::~PlasmaRifle(void) {}

void			PlasmaRifle::attack(void) const {
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
}
