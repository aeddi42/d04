/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:39:38 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:25:27 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

				Character::Character(void) {}

				Character::Character(std::string const & name) : _name(name), _ap(40) {}

				Character::Character(Character const & src) {
	*this = src;
}

Character&		Character::operator=(Character const & rhs) {

	this->_name = rhs._name;
	this->_ap = rhs._ap;

	return *this;
}

				Character::~Character(void) {}

void			Character::recoverAP(void) {

	this->_ap += 10;
	this->_ap = (this->_ap > 40) ? 40 : this->_ap;
}

void			Character::equip(AWeapon *new_weapon) {
	this->_current = new_weapon;
}

void			Character::attack(Enemy *target) {

	if (this->_current != NULL && this->_current->getApcost() <= this->_ap) {

		std::cout << this->_name << " attacks " << target->getType() << " with a " << this->_current->getName() << std::endl;

		this->_current->attack();
		this->_ap -= this->_current->getApcost();
		target->takeDamage(this->_current->getDamage());

		if (target->getHP() <= 0)
			delete target;
	}
}

std::string		Character::getName(void) const {
	return this->_name;
}

AWeapon			*Character::getWeapon(void) const {
	return this->_current;
}

int				Character::getAP(void) const {
	return this->_ap;
}

std::ostream&	operator<<(std::ostream & out, Character const & in) {

	if (in.getWeapon() != NULL)
		out << in.getName() << " has " << in.getAP() << " AP and wields a " << in.getWeapon()->getName() << std::endl;
	else
		out << in.getName() << " has " << in.getAP() << " AP and is unarmed" << std::endl;

	return out;
}
