/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:42 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/09 16:45:27 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RADSCORPION_HPP
# define RADSCORPION_HPP

# include <iostream>
# include "Enemy.hpp"

class	RadScorpion : public Enemy {

	public:

						RadScorpion(void);
						RadScorpion(RadScorpion const & src);
		RadScorpion&	operator=(RadScorpion const & rhs);
						~RadScorpion(void);

};

#endif /* !RADSCORPION_HPP */
