/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:25:22 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:02:07 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

				PowerFist::PowerFist(void) : AWeapon("Power Fist", 8, 50) {}

				PowerFist::PowerFist(PowerFist const & src) : AWeapon() {
	*this = src;
}

PowerFist&	PowerFist::operator=(PowerFist const & rhs) {

	this->_name = rhs.getName();
	this->_apcost = rhs.getApcost();
	this->_damage = rhs.getDamage();

	return *this;
}

				PowerFist::~PowerFist(void) {}

void			PowerFist::attack(void) const {
	std::cout << "* pschhh... SBAM! *" << std::endl;
}
