/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Warrior.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:59 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:28:58 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Warrior.hpp"

				Warrior::Warrior(void) : Character("Rambo") {

	this->_ap = 99999;
	std::cout << "* ADRIEEEEEEEEEENNE *" << std::endl;
}

				Warrior::Warrior(Warrior const & src) : Character() {

	*this = src;
	std::cout << "* ADRIEEEEEEEEEENNE *" << std::endl;
}

Warrior&	Warrior::operator=(Warrior const & rhs) {

	this->_name = rhs.getName();
	this->_ap = rhs.getAP();
	this->_current = rhs.getWeapon();

	return *this;
}

				Warrior::~Warrior(void) {
	std::cout << "* BEUUUUARGH *" << std::endl;
}
