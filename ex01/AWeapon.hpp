/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:14:17 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 11:59:39 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
# define AWEAPON_HPP

# include <iostream>

class	AWeapon {

	protected:

						AWeapon(void);

		std::string		_name;
		int				_apcost;
		int				_damage;

	public:

						AWeapon(std::string const & name, int apcost, int damage);
						AWeapon(AWeapon const & src);
virtual	AWeapon&		operator=(AWeapon const & rhs);
						~AWeapon(void);

		std::string		getName(void) const;
		int				getApcost(void) const;
		int				getDamage(void) const;

virtual	void			attack(void) const = 0;

};

#endif /* !AWEAPON_HPP */
