/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:14:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:02:09 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

			AWeapon::AWeapon(void) {}

			AWeapon::AWeapon(std::string const & name, int apcost, int damage) : _name(name), _apcost(apcost), _damage(damage) {}

			AWeapon::AWeapon(AWeapon const & src) {
	*this = src;
}

AWeapon&	AWeapon::operator=(AWeapon const & rhs) {

	this->_name = rhs._name;
	this->_apcost = rhs._apcost;
	this->_damage = rhs._damage;

	return *this;
}

			AWeapon::~AWeapon(void) {}

std::string	AWeapon::getName(void) const {
	return this->_name;
}

int			AWeapon::getApcost(void) const {
	return this->_apcost;
}

int			AWeapon::getDamage(void) const {
	return this->_damage;
}
