/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FulguroFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:15:40 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:16:53 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FULGUROFIST_HPP
# define FULGUROFIST_HPP

# include "AWeapon.hpp"

class	FulguroFist : public AWeapon {

	public:

						FulguroFist(void);
						FulguroFist(FulguroFist const & src);
		FulguroFist&	operator=(FulguroFist const & rhs);
						~FulguroFist(void);

		void			attack(void) const;

};

#endif /* !FULGUROFIST_HPP */
