/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 16:39:03 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:32:23 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"
#include "Warrior.hpp"
#include "Enemy.hpp"
#include "SuperMutant.hpp"
#include "Zombie.hpp"
#include "RadScorpion.hpp"
#include "PowerFist.hpp"
#include "PlasmaRifle.hpp"
#include "FulguroFist.hpp"
#include "AWeapon.hpp"

int main() {

	Character* zaz = new Character("zaz");
	Warrior* warrior = new Warrior();

	std::cout << *zaz;
	std::cout << *warrior;

	Enemy* a = new Zombie();
	Enemy* b = new RadScorpion();
	Enemy* c = new SuperMutant();

	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();
	AWeapon* ff = new FulguroFist();

	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);
	warrior->equip(ff);
	std::cout << *warrior;

	zaz->attack(b);
	std::cout << *zaz;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	warrior->attack(a);
	std::cout << *warrior;
	warrior->attack(c);
	std::cout << *warrior;
	zaz->attack(b);
	std::cout << *zaz;
	warrior->attack(c);
	std::cout << *warrior;

	return 0;
}
