/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:19:01 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:04:59 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
# define ENEMY_HPP

# include <iostream>

class Enemy {

	protected:

					Enemy(void);

		int			_hp;
		std::string	_type;

	public:

					Enemy(int hp, std::string const & type);
					Enemy(Enemy const & src);
		Enemy&		operator=(Enemy const & rhs);
virtual				~Enemy(void);

		std::string getType() const;
		int			getHP() const;
virtual void		takeDamage(int damage);

};

#endif /* !ENEMY_HPP */
