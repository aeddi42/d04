/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:15:40 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:02:08 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POWERFIST_HPP
# define POWERFIST_HPP

# include "AWeapon.hpp"

class	PowerFist : public AWeapon {

	public:

						PowerFist(void);
						PowerFist(PowerFist const & src);
		PowerFist&		operator=(PowerFist const & rhs);
						~PowerFist(void);

		void			attack(void) const;

};

#endif /* !POWERFIST_HPP */
