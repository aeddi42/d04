/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Warrior.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:42 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:23:20 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WARRIOR_HPP
# define WARRIOR_HPP

# include <iostream>
# include "Character.hpp"

class	Warrior : public Character {

	public:

						Warrior(void);
						Warrior(Warrior const & src);
		Warrior&		operator=(Warrior const & rhs);
						~Warrior(void);

};

#endif /* !WARRIOR_HPP */
