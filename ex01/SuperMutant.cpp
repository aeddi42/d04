/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:20:22 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:09:59 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

				SuperMutant::SuperMutant(void) : Enemy(170, "Super Mutant") {
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

				SuperMutant::SuperMutant(SuperMutant const & src) : Enemy() {

	*this = src;
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant&	SuperMutant::operator=(SuperMutant const & rhs) {

	this->_hp = rhs.getHP();
	this->_type = rhs.getType();

	return *this;
}

				SuperMutant::~SuperMutant(void) {
	std::cout << "Aaargh ..." << std::endl;
}

void			SuperMutant::takeDamage(int damage) {

	damage -= 3;
	if (!(damage < 0))
		this->_hp -= damage;
}
