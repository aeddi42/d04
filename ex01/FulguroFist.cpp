/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FulguroFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:25:22 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:18:24 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FulguroFist.hpp"

				FulguroFist::FulguroFist(void) : AWeapon("Fulguro Fist", 2, 10) {}

				FulguroFist::FulguroFist(FulguroFist const & src) : AWeapon() {
	*this = src;
}

FulguroFist&	FulguroFist::operator=(FulguroFist const & rhs) {

	this->_name = rhs.getName();
	this->_apcost = rhs.getApcost();
	this->_damage = rhs.getDamage();

	return *this;
}

				FulguroFist::~FulguroFist(void) {}

void			FulguroFist::attack(void) const {
	std::cout << "* vrooooooooom... BOUM! *" << std::endl;
}
