/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:42 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:20:00 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

# include <iostream>
# include "Enemy.hpp"

class	Zombie : public Enemy {

	public:

						Zombie(void);
						Zombie(Zombie const & src);
		Zombie&	operator=(Zombie const & rhs);
						~Zombie(void);

};

#endif /* !ZOMBIE_HPP */
