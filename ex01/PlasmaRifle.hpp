/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:15:19 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:00:05 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLASMARIFLE_HPP
# define PLASMARIFLE_HPP

# include "AWeapon.hpp"

class	PlasmaRifle : public AWeapon {

	public:

						PlasmaRifle(void);
						PlasmaRifle(PlasmaRifle const & src);
		PlasmaRifle&	operator=(PlasmaRifle const & rhs);
						~PlasmaRifle(void);

		void			attack(void) const;

};

#endif /* !PLASMARIFLE_HPP */
