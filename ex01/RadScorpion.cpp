/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:59 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:11:19 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.hpp"

				RadScorpion::RadScorpion(void) : Enemy(80, "RadScorpion") {
	std::cout << "* click click click *" << std::endl;
}

				RadScorpion::RadScorpion(RadScorpion const & src) : Enemy() {

	*this = src;
	std::cout << "* click click click *" << std::endl;
}

RadScorpion&	RadScorpion::operator=(RadScorpion const & rhs) {

	this->_hp = rhs.getHP();
	this->_type = rhs.getType();

	return *this;
}

				RadScorpion::~RadScorpion(void) {
	std::cout << "* SPROTCH *" << std::endl;
}
