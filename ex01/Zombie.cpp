/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:33:59 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:20:59 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

				Zombie::Zombie(void) : Enemy(80, "Zombie") {
	std::cout << "* BRAaaaaaiiIINNSSss *" << std::endl;
}

				Zombie::Zombie(Zombie const & src) : Enemy() {

	*this = src;
	std::cout << "* BRAaaaaaiiIINNSSss *" << std::endl;
}

Zombie&	Zombie::operator=(Zombie const & rhs) {

	this->_hp = rhs.getHP();
	this->_type = rhs.getType();

	return *this;
}

				Zombie::~Zombie(void) {
	std::cout << "* I'm deeeeaddd aggaaaiinn *" << std::endl;
}
