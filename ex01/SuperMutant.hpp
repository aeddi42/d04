/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:16:27 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:08:48 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERMUTANT_HPP
# define SUPERMUTANT_HPP

# include <iostream>
# include "Enemy.hpp"

class	SuperMutant : public Enemy {

	public:

						SuperMutant(void);
						SuperMutant(SuperMutant const & src);
		SuperMutant&	operator=(SuperMutant const & rhs);
						~SuperMutant(void);

virtual void			takeDamage(int damage);

};

#endif /* !SUPERMUTANT_HPP */
