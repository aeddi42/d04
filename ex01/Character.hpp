/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 15:39:27 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:25:40 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include <iostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class	Character {

	protected:

						Character(void);

		std::string		_name;
		int				_ap;
		AWeapon			*_current;

	public:

						Character(std::string const & name);
						Character(Character const & src);
		Character&		operator=(Character const & rhs);
						~Character(void);

		void			recoverAP(void);
		void			equip(AWeapon *new_weapon);
		void			attack(Enemy *target);

		std::string		getName(void) const;
		int				getAP(void) const;
		AWeapon			*getWeapon(void) const;

};

std::ostream&	operator<<(std::ostream & out, Character const & in);

#endif /* !CHARACTER_HPP */
