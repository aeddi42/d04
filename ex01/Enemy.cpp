/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 12:25:39 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/10 12:10:07 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

			Enemy::Enemy(void) {}

			Enemy::Enemy(int hp, std::string const & type) : _hp(hp), _type(type) {}

			Enemy::Enemy(Enemy const & src) {
	*this = src;
}

Enemy&		Enemy::operator=(Enemy const & rhs) {

	this->_hp = rhs._hp;
	this->_type = rhs._type;

	return *this;
}

			Enemy::~Enemy(void) {}

void		Enemy::takeDamage(int damage) {

	if (!(damage < 0))
		this->_hp -= damage;
}

std::string	Enemy::getType(void) const {
	return this->_type;
}

int			Enemy::getHP(void) const {
	return this->_hp;
}
