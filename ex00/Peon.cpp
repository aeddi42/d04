/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:50 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:29:06 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

			Peon::Peon(void) {}

			Peon::Peon(std::string name) : Victim(name) {
	std::cout << "Zog zog." << std::endl;
}

			Peon::Peon(Peon const & src) : Victim() {

	*this = src;
	std::cout << "Zog zog." << std::endl;
}

			Peon::~Peon(void) {
	std::cout << "Bleuark..." << std::endl;
}

Peon&		Peon::operator=(Peon const & rhs) {

	this->_name = rhs._name;

	return *this;
}

void		Peon::getPolymorphed(void) const {
	std::cout << this->_name << " has been turned into a pink pony !" << std::endl;
}
