/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:43 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:29:07 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP

# include <iostream>
# include "Victim.hpp"

class	Peon : public Victim {

	public:

						Peon(std::string name);
						Peon(Peon const & src);
						~Peon(void);
		Peon&			operator=(Peon const & rhs);

virtual	void			getPolymorphed(void) const;

	private:

						Peon(void);

};

#endif /* !PEON_HPP */
