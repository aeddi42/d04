/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Warrior.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:43 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:34:02 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WARRIOR_HPP
# define WARRIOR_HPP

# include <iostream>
# include "Victim.hpp"

class	Warrior : public Victim {

	public:

						Warrior(std::string name);
						Warrior(Warrior const & src);
						~Warrior(void);
		Warrior&		operator=(Warrior const & rhs);

virtual	void			getPolymorphed(void) const;

	private:

						Warrior(void);

};

#endif /* !WARRIOR_HPP */
