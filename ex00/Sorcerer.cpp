/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:12 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:11:49 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

			Sorcerer::Sorcerer(void) {}

			Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title) {
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

			Sorcerer::Sorcerer(Sorcerer const & src) {

	*this = src;
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

			Sorcerer::~Sorcerer(void) {
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;
}

Sorcerer&	Sorcerer::operator=(Sorcerer const & rhs) {

	this->_name = rhs._name;
	this->_title = rhs._title;

	return *this;
}

void		Sorcerer::polymorph(Victim const & target) const {
	target.getPolymorphed();
}

std::string	Sorcerer::getName(void) const {
	return this->_name;
}

std::string	Sorcerer::getTitle(void) const {
	return this->_title;
}

std::ostream &			operator<<(std::ostream & out, Sorcerer const & in) {

	out << "I am " << in.getName() << ", " << in.getTitle() << ", and I like ponies !" << std::endl;

	return out;
}
