/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:55 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:35:28 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Warrior.hpp"
#include "Peon.hpp"

int	main(void) {

	Sorcerer robert("Robert", "the Magnificent");

	Victim	jim("Jimmy");
	Peon	joe("Joe");
	Warrior	rocky("Rocky");

	std::cout << robert << jim << joe << rocky;

	robert.polymorph(jim);
	robert.polymorph(joe);
	robert.polymorph(rocky);

	return 0;
}
