/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Warrior.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:50 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:33:41 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Warrior.hpp"

			Warrior::Warrior(void) {}

			Warrior::Warrior(std::string name) : Victim(name) {
	std::cout << "HUCHT !" << std::endl;
}

			Warrior::Warrior(Warrior const & src) : Victim() {

	*this = src;
	std::cout << "HUCHT !" << std::endl;
}

			Warrior::~Warrior(void) {
	std::cout << "ADRIEEENNNE !" << std::endl;
}

Warrior&		Warrior::operator=(Warrior const & rhs) {

	this->_name = rhs._name;

	return *this;
}

void		Warrior::getPolymorphed(void) const {
	std::cout << this->_name << " has been turned into a big buffalo !" << std::endl;
}
