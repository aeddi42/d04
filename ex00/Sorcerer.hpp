/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:06 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:11:50 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP

# include <iostream>
# include "Victim.hpp"

class	Sorcerer {

	public:

						Sorcerer(std::string name, std::string title);
						Sorcerer(Sorcerer const & src);
						~Sorcerer(void);
		Sorcerer&		operator=(Sorcerer const & rhs);

		std::string		getName(void) const;
		std::string		getTitle(void) const;

		void			polymorph(Victim const & target) const;

	private:

						Sorcerer(void);

		std::string		_name;
		std::string		_title;

};

std::ostream &			operator<<(std::ostream & out, Sorcerer const & in);

#endif /* !SORCERER_HPP */
