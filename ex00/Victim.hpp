/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:28 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:25:16 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP

# include <iostream>

class	Victim {

	public:

						Victim(std::string name);
						Victim(Victim const & src);
						~Victim(void);
		Victim&			operator=(Victim const & rhs);

		std::string		getName(void) const;
virtual	void			getPolymorphed(void) const;

	protected:

						Victim(void);
		std::string		_name;

};

std::ostream &			operator<<(std::ostream & out, Victim const & in);

#endif /* !VICTIM_HPP */
