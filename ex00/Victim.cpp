/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 10:15:37 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 22:25:15 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

			Victim::Victim(void) {}

			Victim::Victim(std::string name) : _name(name) {
	std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}

			Victim::Victim(Victim const & src) {

	*this = src;
	std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}
			Victim::~Victim(void) {
	std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
}

Victim&	Victim::operator=(Victim const & rhs) {

	this->_name = rhs._name;
	return *this;
}

void		Victim::getPolymorphed(void) const {
	std::cout << this->_name << " has been turned into a cute little sheep !" << std::endl;
}

std::string	Victim::getName(void) const {
	return this->_name;
}

std::ostream &			operator<<(std::ostream & out, Victim const & in) {

	out << "I am " << in.getName() << " and I like otters !" << std::endl;
	return out;
}
