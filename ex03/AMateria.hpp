/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:44:57 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:56:07 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
# define AMATERIA_HPP

#include <iostream>

class ICharacter;
class AMateria {

	private:
		
		std::string		type_;
		unsigned int	xp_;

	public:

		AMateria(std::string const & type);
AMateria& operator+=(int xp);
virtual	~AMateria();

		std::string const & getType() const;
		unsigned int getXP() const;

virtual	AMateria* clone() const = 0;
virtual	void use(ICharacter& target);

};

#endif /* !AMATERIA_HPP */
