/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:46:04 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:26:40 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"

				MateriaSource::MateriaSource(void) {}

				MateriaSource::MateriaSource(MateriaSource const & src) {
	*this = src;
}

MateriaSource&	MateriaSource::operator=(MateriaSource const & rhs) {
	return *this;
}

				MateriaSource::~MateriaSource(void) {}

void			MateriaSource::learnMateria(AMateria* mat) {

	int i;

	for(i = 0; i < 4; i++) {
		if (this->_source[i] == NULL) {
			this->_source[i] = mat->clone();
			return;
		}
	}
}

AMateria*		MateriaSource::createMateria(std::string const & type) {

	int i;

	for(i = 0; i < 4; i++) {
		if (this->_source[i]->getType() == type)
			return this->_source[i]->clone();
	}
	return 0;
}
