/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 12:47:10 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:56:40 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICE_HPP
# define ICE_HPP

#include "AMateria.hpp"
#include "ICharacter.hpp"

class Ice : public AMateria {

	public:

		Ice(void);
		Ice(Ice const & src);
		Ice& operator=(Ice const & rhs);
		~Ice(void);

		AMateria* clone() const;
		void use(ICharacter& target);

};

#endif /* !ICE_HPP */
