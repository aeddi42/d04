/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:46:34 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 17:16:30 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include "Character.hpp"
#include "Cure.hpp"
#include "Ice.hpp"

int main()
{
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());
	src->learnMateria(new Ice());

	ICharacter* zaz = new Character("zaz");

	AMateria* tmp;
	AMateria* tmp2;
	AMateria* tmp3;

	tmp = src->createMateria("ice");
	tmp2 = tmp->clone();
	tmp3 = src->createMateria("cure");
	zaz->equip(tmp);
	zaz->equip(tmp2);
	zaz->equip(tmp3);

	ICharacter* bob = new Character("bob");

	zaz->use(0, *bob);
	zaz->unequip(0);
	zaz->use(0, *bob);
	zaz->equip(tmp);
	zaz->use(0, *bob);

	std::cout << tmp->getXP() << std::endl;
	std::cout << tmp2->getXP() << std::endl;
	std::cout << tmp3->getXP() << std::endl;

	zaz->use(1, *bob);
	zaz->use(2, *bob);
	zaz->equip(tmp2);
	zaz->use(2, *bob);

	std::cout << tmp->getXP() << std::endl;
	std::cout << tmp2->getXP() << std::endl;
	std::cout << tmp3->getXP() << std::endl;

	delete bob;
	delete zaz;
	delete src;
}
