/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:45:29 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:56:55 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURE_HPP
# define CURE_HPP

#include "AMateria.hpp"
#include "ICharacter.hpp"

class Cure : public AMateria {

	public:

		Cure(void);
		Cure(Cure const & src);
		Cure& operator=(Cure const & rhs);
		~Cure(void);

		AMateria* clone() const;
		void use(ICharacter& target);

};

#endif /* !CURE_HPP */
