/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 12:40:03 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 15:42:05 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"

					AMateria::AMateria(std::string const & type) : type_(type), xp_(0) {}

					AMateria::~AMateria() {}

std::string const & AMateria::getType() const {
	return this->type_;
}

AMateria& 			AMateria::operator+=(int xp) {

	this->xp_ += xp;
	return *this;
}

unsigned int		AMateria::getXP() const {
	return this->xp_;
}

void 				AMateria::use(ICharacter& target) {
	(void)target;
	this->xp_ += 10;
}
