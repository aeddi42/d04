/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:45:17 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 15:42:59 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"

			Ice::Ice(void) : AMateria("ice") {}

			Ice::Ice(Ice const & src) : AMateria("ice") {
	*this = src;
}

Ice&		Ice::operator=(Ice const & rhs) {

	(void)rhs;
	return *this;
}

			Ice::~Ice(void) {}

void		Ice::use(ICharacter& target) {
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
	*this += 10;
}

AMateria*	Ice::clone() const {
	return new Ice;
}
