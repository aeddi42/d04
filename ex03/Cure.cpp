/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 15:05:33 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:57:09 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

			Cure::Cure(void) : AMateria("cure") {}

			Cure::Cure(Cure const & src) : AMateria("cure") {
	*this = src;
}

Cure&		Cure::operator=(Cure const & rhs) {

	(void)rhs;
	return *this;
}

			Cure::~Cure(void) {}

void		Cure::use(ICharacter& target) {
	std::cout << "* heals " << target.getName() << "'s wounds *" << std::endl;
	*this += 10;
}

AMateria*	Cure::clone() const {
	return new Cure;
}
