/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 10:45:45 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:55:34 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

#include "ICharacter.hpp"

class Character : public ICharacter {

	public:
		Character(std::string const & name);
		Character(Character const & src);
		Character& operator=(Character const & rhs);
		~Character(void);

		void unequip(int idx);
		void use(int idx, ICharacter& target);
		std::string const & getName() const;
		void equip(AMateria* m);

	private:
		Character(void);

		std::string	_name;
		AMateria*	_current[4];

};

#endif /* !CHARACTER_HPP */
