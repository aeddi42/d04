/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 15:15:04 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 16:32:22 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

			Character::Character(void) {}

			Character::Character(std::string const & name) : _name(name) {

	int i;

	for(i = 0; i < 4; i++)
		this->_current[i] = NULL;
}

			Character::Character(Character const & src) {
	*this = src;
}

			Character::~Character(void) {}

Character&	Character::operator=(Character const & rhs) {

	int i;

	this->_name = rhs.getName();
	for(i = 0; i < 4; i++) {
		if (this->_current[i] != NULL) {
			delete this->_current[i];
			this->_current[i] = NULL;
		}
	}

	for(i = 0; i < 4; i++) {
		if (rhs._current[i] != NULL)
			this->_current[i] = rhs._current[i]->clone();
	}

	return *this;
}

void		Character::unequip(int idx) {

	if (idx < 4 && idx >= 0)
		this->_current[idx] = NULL;
}

void		Character::equip(AMateria* mat) {

	int i;

	for(i = 0; i < 4; i++) {
		if (this->_current[i] == NULL) {
			this->_current[i] = mat;
			return;
		}
	}
}

void		Character::use(int idx, ICharacter& target) {

	if (idx < 4 && idx >= 0) {
		if (this->_current[idx] != NULL)
			this->_current[idx]->use(target);
	}
}

std::string	const & Character::getName(void) const {
	return this->_name;
}
