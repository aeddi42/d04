/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 14:07:57 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 15:39:34 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASSAULTTERMINATOR_HPP
# define ASSAULTTERMINATOR_HPP

# include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine {

	public:

						AssaultTerminator(void);
						AssaultTerminator(AssaultTerminator const & src);
						AssaultTerminator& operator=(AssaultTerminator const & rhs);
						~AssaultTerminator(void);

		ISpaceMarine*	clone() const;
		void			battleCry() const;
		void			rangedAttack() const;
		void			meleeAttack() const;

};

#endif /* !ASSAULTTERMINATOR_HPP */
