/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 15:27:33 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 16:03:22 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"
#include <iostream>

				TacticalMarine::TacticalMarine(void) {
	std::cout << "Tactical Marine ready for battle" << std::endl;
}

				TacticalMarine::TacticalMarine(TacticalMarine const & src) {
	std::cout << "Tactical Marine ready for battle" << std::endl;
	*this = src;
}

TacticalMarine& TacticalMarine::operator=(TacticalMarine const & rhs) {

	(void)rhs;
	return *this;
} 

				TacticalMarine::~TacticalMarine(void) {
	std::cout << "Aaargh ..." << std::endl;
}

ISpaceMarine*	TacticalMarine::clone() const {
	return new TacticalMarine;
}

void			TacticalMarine::battleCry() const {
	std::cout << "For the holy PLOT !" << std::endl;
}

void			TacticalMarine::rangedAttack() const {
	std::cout << "* attacks with bolter *" << std::endl;
}

void			TacticalMarine::meleeAttack() const {
	std::cout << "* attacks with chainsword *" << std::endl;
}
