/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 14:08:06 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 16:56:14 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP

# include "ISquad.hpp"

class Squad : public ISquad {

	public:

						Squad(void);
						Squad(Squad const & src);
		Squad&			operator=(Squad const & rhs);
						~Squad(void);

		int				getCount(void) const;
		ISpaceMarine*	getUnit(int);

		int				push(ISpaceMarine* member);


	private:

		int				_count;
		ISpaceMarine**	_squad;
		

};

#endif /* !SQUAD_HPP */
