/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 15:40:28 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 17:03:16 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"
#include "TacticalMarine.hpp"
#include "Squad.hpp"
#include <iostream>

int main()
{
	ISpaceMarine* bob = new TacticalMarine;
	ISpaceMarine* jim = new AssaultTerminator;
	ISpaceMarine* jim2 = new AssaultTerminator(*(AssaultTerminator *)jim);

	std::cout << std::endl << "Create first squad:" << std::endl;
	ISquad* vlc = new Squad;
	vlc->push(bob);
	vlc->push(jim);

	std::cout << std::endl << "Create second squad:" << std::endl;
	Squad* vlc2 = new Squad(*(Squad *)vlc);

	std::cout << std::endl << "Create third squad:" << std::endl;
	Squad*	vlc3 = new Squad;
	*vlc3 = *vlc2;
	vlc3->push(jim2);

	std::cout << std::endl << "Display first squad:" << std::endl;
	for (int i = 0; i < vlc->getCount(); ++i) {
		ISpaceMarine* cur = vlc->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Display second squad:" << std::endl;
	for (int i = 0; i < vlc2->getCount(); ++i) {
		ISpaceMarine* cur = vlc2->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Display third squad:" << std::endl;
	for (int i = 0; i < vlc3->getCount(); ++i) {
		ISpaceMarine* cur = vlc3->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Delete first squad:" << std::endl;
	delete vlc;

	std::cout << std::endl << "Delete second squad:" << std::endl;
	delete vlc2;

	std::cout << std::endl << "Delete third squad:" << std::endl;
	delete vlc3;

	return 0;
}
