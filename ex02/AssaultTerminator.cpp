/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 15:36:12 by plastic           #+#    #+#             */
/*   Updated: 2015/04/11 10:44:12 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"
#include <iostream>

					AssaultTerminator::AssaultTerminator(void) {
	std::cout << "* teleports from space *" << std::endl;
}

					AssaultTerminator::AssaultTerminator(AssaultTerminator const & src) {
	std::cout << "* teleports from space *" << std::endl;
	*this = src;
}

AssaultTerminator&	AssaultTerminator::operator=(AssaultTerminator const & rhs) {

	(void)rhs;
	return *this;
} 

					AssaultTerminator::~AssaultTerminator(void) {
	std::cout << "I’ll be back ..." << std::endl;
}

ISpaceMarine*		AssaultTerminator::clone() const {
	return new AssaultTerminator;
}

void				AssaultTerminator::battleCry() const {
	std::cout << "This code is unclean. PURIFY IT !" << std::endl;
}

void				AssaultTerminator::rangedAttack() const {
	std::cout << "* does nothing *" << std::endl;
}

void				AssaultTerminator::meleeAttack() const {
	std::cout << "* attacks with chainfists *" << std::endl;
}
