/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 14:08:16 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 15:38:02 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TACTICALMARINE_HPP
# define TACTICALMARINE_HPP

# include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine {

	public:

						TacticalMarine(void);
						TacticalMarine(TacticalMarine const & src);
						TacticalMarine& operator=(TacticalMarine const & rhs);
						~TacticalMarine(void);

		ISpaceMarine*	clone() const;
		void			battleCry() const;
		void			rangedAttack() const;
		void			meleeAttack() const;

};

#endif /* !TACTICALMARINE_HPP */
