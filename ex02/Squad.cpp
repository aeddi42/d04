/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 14:03:41 by plastic           #+#    #+#             */
/*   Updated: 2015/04/10 16:56:13 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"
#include "TacticalMarine.hpp"
#include <stdlib.h>

				Squad::Squad(void) : _count(0), _squad(NULL) {}

				Squad::Squad(Squad const & src) {
	*this = src;
}

Squad&			Squad::operator=(Squad const & rhs) {

	int	count;

	for (count = 0; count < this->_count; count++)
		delete this->_squad[count];
	delete[] this->_squad;

	this->_count = rhs._count;
	this->_squad = new ISpaceMarine*[this->_count];
	for (count = 0; count < this->_count; count++)
		this->_squad[count] = rhs._squad[count]->clone();

	return *this;
}

				Squad::~Squad(void) {

	int	count;

	for (count = 0; count < this->_count; count++)
		delete this->_squad[count];
	delete[] this->_squad;
}

int				Squad::getCount(void) const {
	return this->_count;
}

ISpaceMarine*	Squad::getUnit(int n) {

	if (n < this->_count && n >= 0)
		return this->_squad[n];
	return NULL;
}

int				Squad::push(ISpaceMarine* member) {

	ISpaceMarine**	tmp;
	int				count;
	
	tmp = new ISpaceMarine*[this->_count + 1];
	for (count = 0; count < this->_count; count++)
		tmp[count] = this->_squad[count];
	tmp[this->_count] = member;
	delete[] this->_squad;
	this->_squad = tmp;

	return ++this->_count;
}
